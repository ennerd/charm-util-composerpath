Charm/Util/ComposerPath
=======================

Most projects store the `composer.json` file in the root of the project. This library provides a single function:

| Detecting the project root by calling `$root = \Charm\Util\ComposerPath::get();`.


Normal Usage
------------

```
// Let's store the root path in a PHP constant
define('ROOT', Charm\Util\ComposerPath::get());

// If composer is not used, the value is `null`
if (null === \ROOT) {
    throw new \Exception("This project requires 'composer'.");
}

// Do your thing:
$databaseConfig = require(ROOT.'/config/database.php');
```


Command Line Snippet
--------------------

If you are looking to find the `vendor/autoload.php` file for a shell command, you should instead invoke the following one-liner or copy the `autoload-autoloader.php` script from this library.

```
/*
 * Find the path to `vendor/autoload.php`.
 */
require realpath(__DIR__.'/../../../vendor/autoload.php') ?: realpath(__DIR__.'/../vendor/autoload.php') ?: 'vendor/autoload.php';
```

This variant assumes that your script is in a sub-directory of your library. Remove or add `/..` as needed if your scipt
