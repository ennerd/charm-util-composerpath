<?php
declare(strict_types=1);

namespace Charm\Util;

use Composer\Autoload\ClassLoader;
use ReflectionClass;

/**
 * Utility which provides the path where composer.json is installed.
 */
class ComposerPath
{
    private static string $path = '';

    /**
     * Returns the path to the composer.json file. Returns null
     * if Composer is not loaded.
     *
     * @psalm-external-mutation-free
     * @psalm-suppress all
     */
    public static function get(): ?string
    {
        if ('' !== static::$path) {
            return static::$path;
        }
        /*
         * @psalm-suppress ImpureFunctionCall
         */
        if (!class_exists(ClassLoader::class, false)) {
            return static::$path = null;
        }
        /*
         * @psalm-suppress ImpureFunctionCall
         */
        if (class_exists(ReflectionClass::class)) {
            $ref = new ReflectionClass(ClassLoader::class);

            /*
             * @psalm-suppress ImpureMethodCall
             */
            return static::$path = \dirname($ref->getFileName(), 3);
        }

        /**
         * Find the shortest path from the ClassLoader. This should correspond
         * with the topmost ./vendor directory.
         */
        $shortest = \PHP_INT_MAX;
        $candidate = null;
        foreach (ClassLoader::getRegisteredLoaders() as $path => $loader) {
            if ('vendor' !== substr($path, -6)) {
                continue;
            }
            $length = \strlen($path);
            if ($length < $shortest) {
                $candidate = $path;
                $shortest = $length;
            }
        }

        if ($candidate) {
            /**
             * @psalm-suppress ImpureFunctionCall
             */
            $path = realpath(\dirname($candidate).'/composer.json');
            if ($path) {
                return static::$path = \dirname($path);
            }
        }

        return static::$path = null;
    }
}
