<?php
declare(strict_types=1);

/*
 * Find the path to `vendor/autoload.php`.
 */
require realpath(__DIR__.'/../../../vendor/autoload.php') ?: realpath(__DIR__.'/../vendor/autoload.php') ?: 'vendor/autoload.php';
