<?php
declare(strict_types=1);
require __DIR__.'/vendor/autoload.php';

if (__DIR__ !== Charm\Util\ComposerPath::get()) {
    echo "Test failed\n";
    exit(1);
}
echo "Success\n";
